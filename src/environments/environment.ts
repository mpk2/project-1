// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA6qTDF3sjutHbJ4oBW3WP5E_lJCh4Za4E",
    authDomain: "finals-es.firebaseapp.com",
    projectId: "finals-es",
    storageBucket: "finals-es.appspot.com",
    messagingSenderId: "112424417301",
    appId: "1:112424417301:web:0061152e34477ebafdb61c",
    measurementId: "G-NEWTEXHZYY"
  }
}


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
